<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'winner'        
    ];
    
    public function prizes() {
        return $this->hasMany('App\Move');
    }
}
