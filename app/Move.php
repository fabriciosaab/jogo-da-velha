<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
    protected $fillable = [
        'player',
        'position',
        'time'
    ];
    
    public function game() {
        return $this->belongsTo('App\Game');
    }
}
