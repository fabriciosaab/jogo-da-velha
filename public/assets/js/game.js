function makeBoard() {
    $("#board").html(
        '<img class="img-fluid" src="assets/imgs/board.png">'+
        '<div id="A0" position="A0" onclick="playerMove(this)"></div>'+
        '<div id="A1" position="A1" onclick="playerMove(this)"></div>'+
        '<div id="A2" position="A2" onclick="playerMove(this)"></div>'+
        '<div id="A3" position="A3" onclick="playerMove(this)"></div>'+
        '<div id="A4" position="A4" onclick="playerMove(this)"></div>'+
        '<div id="A5" position="A5" onclick="playerMove(this)"></div>'+
        '<div id="A6" position="A6" onclick="playerMove(this)"></div>'+
        '<div id="A7" position="A7" onclick="playerMove(this)"></div>'+
        '<div id="A8" position="A8" onclick="playerMove(this)"></div>'
    );
}
function startGame() {
    console.log("Jogo iniciado");
    makeBoard();
    TweenMax.to($("#fire"), .3, { css: { display: "none", opacity: 0 } });
    TweenMax.to($("#startModal .modal-dialog"), .8, { css: { top: "-=500" } });        
    TweenMax.to($("#startModal"), .8, { css: { display: "none", opacity: 0 } });
    step = 1;
    diff = $("#diff").val();    
    gameMatrix = { A0: null, A1: null, A2: null, A3: null, A4: null, A5: null, A6: null, A7: null, A8: null };    
    movements = [];
    $("#bdSave").html("");
}

function endGame() {
    TweenMax.to($("#endModal"), .8, { css: { display: "block", opacity: 1 } });
    TweenMax.to($("#endModal .modal-dialog"), .8, { css: { top: "+=500" } });                
    sendResult();
}

function resetGame() {
    makeBoard();
    if(step <= 0) {
        TweenMax.to($("#endModal .modal-dialog"), .8, { css: { top: "-=500" } });        
        TweenMax.to($("#endModal"), .8, { css: { display: "none", opacity: 0 } });    
    }
    TweenMax.to($("#startModal"), .8, { css: { display: "block", opacity: 1 } });
    TweenMax.to($("#startModal .modal-dialog"), .8, { css: { top: "+=500" } });                
}

// Player movement
function playerMove(field) {
    // Check if the game isn't finished
    if (step == -1) { alert("Reset e jogue novamente"); return; }
    
    // Check if field is avaliable
    if (gameMatrix[$(field).attr("position")] != null ) { return; }                        
    
    // Position attribute of clicked field
    gameMatrix[$(field).attr("position")] = "x";

    // Insert img
    $(field).append('<img src="./assets/imgs/x.png"/>');
    
    // Save the movement to send to API
    move = {player: "x", position: $(field).attr("position"), time: Date($.now())};    
    movements.push({move});

    // Check if wins
    if (winner(gameMatrix)) {
        console.log("Você venceu!!"); 
        $("#winner").text("Você venceu!!!");
        win = "x";
        endGame();
        step = -1;
        return;
    }

    if (step > 4) {
        console.log("Não houve vencedor.");
        $("#winner").text("Empatou");
        win = "none";
        endGame();
        step = -1;
        return;
    }    
    
    // CPU Movement        
    cpu(gameMatrix);

    step++
}

function getRandomPosition(gameMatrix) {
    do {
        position = "A" + Math.floor(Math.random() * ((8-1)+1) + 1);
    } while(gameMatrix[position] != null);
    
    return position;
}

function cpu(gameMatrix) {    
    switch(diff) {
        // Easy just put random position
        case '1':            
            gameMatrix[getRandomPosition(gameMatrix)] = "o";
            break;
        // Medium use some intelligence 
        case '2':
            position = cpuMove(gameMatrix);
            gameMatrix[position] = "o";
            break
        // Hard use a lot of intelligence
        case '4':
            position = cpuMove(gameMatrix);
            gameMatrix[position] = "o";
            break;
        // It's a troll
        case '5':
            do {
                position = getRandomPosition(gameMatrix);
                gameMatrix[position] = "o";
                $('#'+position).append('<img src="./assets/imgs/o.png"/>');            
            } while(!winner(gameMatrix));

            TweenMax.to($("#fire"), .3, { css: { display: "block", opacity: 1 } });
            $("#winner").text("Te avisei para nem tentar");
            win = "troll";
            endGame();
            step = -2;
            return;            
    }

    // Insert img
    $('#'+position).append('<img src="./assets/imgs/o.png"/>');

    // Save the movement to send to API
    move = {player: "o", position: position, time: Date($.now())};    
    movements.push({move});
    
    if (winner(gameMatrix)) {
        console.log("Você Perdeu!!!");
        $("#winner").text("Você Perdeu!!!");
        win = "o";        
        step = -2;
        endGame();
    }
    return;
}

// Check if there is a winner
function winner(gameMatrix) {
    if (
        (gameMatrix["A0"] != null && gameMatrix["A0"] == gameMatrix["A3"] && gameMatrix["A0"] == gameMatrix["A6"]) || 
        
        (gameMatrix["A0"] != null && gameMatrix["A0"] == gameMatrix["A1"] && gameMatrix["A0"] == gameMatrix["A2"]) ||

        (gameMatrix["A0"] != null && gameMatrix["A0"] == gameMatrix["A4"] && gameMatrix["A0"] == gameMatrix["A8"]) ||

        (gameMatrix["A1"] != null && gameMatrix["A1"] == gameMatrix["A4"] && gameMatrix["A1"] == gameMatrix["A7"]) ||

        (gameMatrix["A2"] != null && gameMatrix["A2"] == gameMatrix["A5"] && gameMatrix["A2"] == gameMatrix["A8"]) ||

        (gameMatrix["A2"] != null && gameMatrix["A2"] == gameMatrix["A4"] && gameMatrix["A2"] == gameMatrix["A6"]) ||

        (gameMatrix["A3"] != null && gameMatrix["A3"] == gameMatrix["A4"] && gameMatrix["A3"] == gameMatrix["A5"]) ||

        (gameMatrix["A6"] != null && gameMatrix["A6"] == gameMatrix["A7"] && gameMatrix["A6"] == gameMatrix["A8"])
    )
        return true;
    else	
        return false;
}

// get position for move.
function comp_move(gameMatrix,player,weight,depth) {
    var cost;
    var bestcost = -2;
    var position;
    var newplayer;

    if (player == "x") newplayer = "o"; else newplayer = "x";			

    if (depth == diff) return 0;

    if (winner(gameMatrix)) return 1;

    for (var i = 0; i < 9; ++i) {
        position ='A'+i;

        if (gameMatrix[position] != null) continue;

        gameMatrix[position] = player;

        cost = comp_move(gameMatrix,newplayer, -weight, depth+1);

        if (cost > bestcost) {
            bestcost = cost;
            if (cost == 1) i = 9;
        }

        gameMatrix[position] = null;
    }

    if (bestcost == -2) bestcost = 0;

    return(-bestcost);
}

// get position for move.
function cpuMove(gameMatrix) {
    var cost;
    var bestcost = -2;
    bestmove = "";

    // Random first move
    if (step == 1) {
        console.log(getRandomPosition(gameMatrix));
        return getRandomPosition(gameMatrix);
    }
    for (var i=0; i<9; ++i) {
        localposition = 'A'+i;
        if (gameMatrix[localposition] != null) {
            continue;
        }

        gameMatrix[localposition] = "o";

        cost = comp_move(gameMatrix,"x", -1, 0);

        if (cost > bestcost) {
            if (cost == 1) { 
                i = 9; 
            }
            bestmove = localposition;
            bestcost = cost;
        }
        gameMatrix[localposition] = null;
    }
    return bestmove;
}

// Send the result to API
function sendResult(){
    result = {winner: win, movements: movements}
    
    $.ajax({
        type: "GET",
        url: "http://localhost:8000/store",
        data: result,
        success: function( data )
        {
            if(data == 'saved') {
                console.log("Resultados Salvos");
                $("#bdSave").html("<p>Jogo Salvo no Banco</p>");
            } else {
                console.log("Não foi possível salvar");
                $("#bdSave").html("<p>Não foi possível salvar no Banco</p>");
            }
        },
        error: function() {
            console.log("Não foi possível salvar");
            $("#bdSave").html("<p>Não foi possível salvar no Banco</p>");
        }
    });
}