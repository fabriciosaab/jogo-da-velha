<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jogo da Velha</title>
    <script type="text/javascript" charset="utf-8" src="assets/vendors/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/vendors/green-sock/jquery.gsap.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="assets/vendors/green-sock/TweenMax.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/game.js"></script>
    <link rel="stylesheet" href="assets/vendors/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>
    <article id="main" class="text-center">
        <div id="board" class="mx-auto"></div>
        <button id="btn-reset" class="btn btn-primary" onclick="resetGame()">Resetar</button>
        <div id="bdSave"></div>    
        <div id="startModal">
            <div class="modal">
               <div id="BGModal"></div>
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Jogo da Velha</h5>                        
                    </div>
                    <div class="modal-body">
                        <form>                            
                            <div class="form-group">
                                <label for="diff">Escolha a dificuldade:</label>
                                <select class="form-control" id="diff">
                                    <option value="1">Fácil</option>
                                    <option value="2">Médio</option>
                                    <option value="4">Difícil</option>
                                    <option value="5">Impossível (nem tente...)</option>                                    
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-primary" onclick="startGame()">Jogar</button>
                    </div>                    
                </div>
              </div>
            </div>
        </div>

        <div id="endModal">            
            <div class="modal">
                <div id="BGModal"></div>
                <img id="fire" src="assets/imgs/fire.jpg">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Jogo da Velha</h5>                        
                    </div>
                    <div class="modal-body">
                        <h1 id="winner"></h1>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-primary" onclick="resetGame()">Jogar Novamente</button>
                    </div>                                    
                </div>
            </div>
        </div>
    </article>
</body>
</html>