# Jogo da Velha

## Requisitos
- PHP >= 7.1.3 
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Terminal com acesso ao PHP
- Banco de dados MySql
- GIT

## Instalação
1. Clonar o Repositório
`git clone https://bitbucket.org/fabriciosaab/jogo-da-velha.git`

2. Ir ao diretório
`cd jogo-da-velha/`

3. Instalar as Dependências
`composer install`

4. Copiar e Renomear o arquivo `.env.example` para `.env`
`cp .env.example .env`

5. Preencher o arquivo `.env` com as credênciais do banco

6. Instalar o banco
`php artisan migrate`

7. Rodar o servidor local
`php -S localhost:8000 -t public`

8. Acessar no navegador
`localhost:8000`


**Desenvolvido por fabricio.saab@me.com**